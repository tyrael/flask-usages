#!/usr/bin/env python
# encoding: utf-8
import time
from application.extensions import db


class Todo(db.Model):
    __tablename__ = "todo"
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(512), unique=True)
    date_added = db.Column(db.TIMESTAMP, default=time.time)

    def __repr__(self):
        return '<Todo: {}>'.format(self.content[:25])
