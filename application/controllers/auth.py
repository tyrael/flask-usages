#!/usr/bin/env python
# encoding: utf-8
from flask.views import MethodView
from flask_login import login_user

from application.models import User


class AuthView(MethodView):
    def get(self):
        user = User.query.all()[0]
        print("auth get")
        login_user(user)
        print("user_login")
        return "ok"

    def post(self):
        return "ok"
