#!/usr/bin/env python
# encoding: utf-8
from flask import request, jsonify
from flask.views import MethodView

from application.extensions import db
from application.models.todo import Todo


class TodoView(MethodView):
    def get(self):
        todo_id = request.args.get('todo_id')
        todo = Todo.query.filter_by(id=todo_id).first()
        if todo:
            return jsonify(**{todo_id: todo.content})
        else:
            return jsonify(**{})

    def post(self):
        data = request.get_json()
        todo = Todo(content=data.get('content'))
        db.session.add(todo)
        db.session.commit()
        return jsonify(todo_id=todo.id, content=todo.content)
