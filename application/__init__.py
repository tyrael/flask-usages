#!/usr/bin/env python
# encoding: utf-8
import os
import sys
import logging
import logging.handlers

from flask import Flask
from flask_login import user_logged_in

from config import load_config
from application.models import User
from application.extensions import db, migrate, login_manager
from application.controllers import TodoView, AuthView


# convert python's encoding to utf8
try:
    reload(sys)
    sys.setdefaultencoding('utf8')
except (AttributeError, NameError):
    pass


def create_app(mode):
    """Create Flask app."""
    print ("mode: {}".format(mode))
    config = load_config(mode)

    app = Flask(__name__)
    app.config.from_object(config)

    if not hasattr(app, 'production'):
        app.production = not app.debug and not app.testing

    # Registe components
    configure_logging(app)
    registe_view(app)
    registe_extensions(app)
    registe_signal_handlers(app)

    return app


def registe_view(app):
    app.add_url_rule('/todo', view_func=TodoView.as_view('todo'))
    app.add_url_rule('/auth', view_func=AuthView.as_view('auth'))


def registe_extensions(app):
    """config extensions"""
    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.filter_by(id=user_id).first()


def registe_signal_handlers(app):
    @user_logged_in.connect_via(app)
    def after_user_login(sender, **kwargs):
        print ("signal -- user login")


def configure_logging(app):
    """config logging"""
    logging.basicConfig()
    if app.config.get('TESTING'):
        app.logger.setLevel(logging.INFO)
        return
    elif app.config.get('DEBUG'):
        app.logger.setLevel(logging.DEBUG)
    else:
        app.logger.setLevel(logging.INFO)

    # make dir if /tmp/logs not exists
    if not os.path.isdir(app.config["LOG_DIR"]):
        os.mkdir(app.config["LOG_DIR"])
    info_log = os.path.join(app.config["LOG_DIR"], "running.info")
    info_file_handler = logging.handlers.RotatingFileHandler(
        info_log, maxBytes=104857600, backupCount=2)
    info_file_handler.setLevel(logging.DEBUG)
    info_file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]')
    )
    app.logger.addHandler(info_file_handler)
